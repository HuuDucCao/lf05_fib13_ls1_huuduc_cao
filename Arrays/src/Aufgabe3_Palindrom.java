// Aufgabe 3 
// Im Programm „Palindrom“ werden über die Tastatur 5 Zeichen eingelesen und in einem geeigneten Array gespeichert.
// Ist dies geschehen, wird der Arrayinhalt in umgekehrter Reihenfolge (also von hinten nach vorn) auf der Konsole ausgegeben.

import java.util.Scanner;

public class Aufgabe3_Palindrom {
    public static void main(String[] args) {
        
        Scanner textEingabe = new Scanner(System.in);   // Scanner Initialisieren
        boolean flagVariable = true;                                  // Initialisierung einer flagVariable für erneute Eingabe



        while(flagVariable == true) {                                   // prüfen ob flag dem boolean Wert true entspricht

            System.out.println("Bitte geben Sie etwas mit 5 Zeichen ein: ");
            String eingabe = textEingabe.next();                                                        // Eingabe vom User

            char[] zeichenEinzeln = eingabe.toCharArray();                                      // erstellen eines Arrays aus den Zeichen des Users

            if (zeichenEinzeln.length == 5) {                                                            // Pruefung ob Laenge des Arrays 5 ist
                
                System.out.println("Ihre Eingabe lautet: "+ eingabe);                       // Ausgabe der Eingabe

                // Ausgabe der einzelnen Zeichen in umgekehrter Reihenfolge
                System.out.print("Das entsprechende Palinndrom lautet: ");
                for (int i = zeichenEinzeln.length; i <= zeichenEinzeln.length && i != 0; i--) {
                    System.out.print(zeichenEinzeln[i - 1]);
                }

                flagVariable = false;   // flagVariable auf false setzen damit aus Schleife gesprungen wird
            }

            // Ausgabe für den Fall dass keine 5 Zeichen eingegeben werden
            else {
                    System.out.println("Ihre Eingabe hat keine 5 Zeichen!");
                }


        }
    }
}

// ================================================================================================================================
        
// Alternative Lösung

        // Scanner tastatur = new Scanner(System.in);
        // System.out.println("Bitte geben Sie 5 Zeichen ein: ");
        // String eingabe = tastatur.next();
        // char[] eingabeZeichen = eingabe.toCharArray();

        // System.out.println("Die umgekehrte Reihenfolge lautet: ");
        // for (int i = eingabeZeichen.length; i <= eingabeZeichen.length && i != 0; i--){
        //     System.out.print(eingabeZeichen[i - 1] + " ");
        // }

