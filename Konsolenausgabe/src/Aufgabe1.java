
public class Aufgabe1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Variablen wurden definiert
		int alter = 19;
		String name = "Duc";
		// Ausgabecommands
		System.out.print("Dies ist ein Platzhalter! ");
		System.out.println("3 + 4 = 7!");
		System.out.println("Dieses Projekt wurde im \"IT-Kurs\" geschrieben!");
		System.out.println("Hallo! Mein name ist " + name + " und ich bin " + alter + " Jahre alt!");
		// Der Unterschied zwischen dem print- und dem println-Befehl ist, dass bei println ein Zeilenumbruch geschieht 
	}

}
