import java.util.Scanner;

public class Aufgabe_5_BMI {

	public static void main(String[] args) {
		int gewicht; 
		double koerpergroesse;
		String geschlecht;
		double summe;
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bitte geben Sie Ihren Gewicht ein: ");
		gewicht = myScanner.nextInt();
		System.out.println("Bitte geben Sie Ihre Körpergröße in cm ein: ");
		koerpergroesse = myScanner.nextDouble();
		System.out.println("Bitte geben Sie Ihr Geschlecht ein: ");
		geschlecht = myScanner.next();
		
		koerpergroesse = koerpergroesse / 100;
		
		System.out.print("Ihr BMI beträgt: ");
		summe = (bmi(gewicht, koerpergroesse));
		System.out.println(summe);

		if (geschlecht.equals("m")) {
			if (summe < 20) {
				System.out.println("Sie befinden sich im Untergewicht.");
			}
			else if (summe >= 20 && summe <= 25) {
				System.out.println("Sie befinden sich im Normalgewicht.");
			}
			else {
				System.out.println("Sie befinden sich im Übergewicht.");
			}
		}
		
		if (geschlecht.equals("w")) {
			if (summe < 19) {
				System.out.println("Sie befinden sich im Untergewicht.");
			}
			else if (summe >= 19 && summe <= 24) {
				System.out.println("Sie befinden sich im Normalgewicht.");
			}
			else {
				System.out.println("Sie befinden sich im Übergewicht.");
			}
		}
	}
	static double bmi(double a, double b) {
		double bmi = (a / (b * b));
		return bmi;
	}

}
