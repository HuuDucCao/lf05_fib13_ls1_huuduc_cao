import java.util.Scanner;

public class Aufgabe_8_Schaltjahr {

	public static void main(String[] args) {
		int jahreszahl;
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bitte geben Sie eine Jahreszahl ein: ");
		jahreszahl = myScanner.nextInt();
		
		if (jahreszahl % 4 == 0) {
			if (jahreszahl % 100 == 0) {
				if(jahreszahl % 400 == 0) {
					System.out.println("Es ist ein Schaltjahr.");
				}
				else {
					System.out.println("Es ist kein Schaltjahr.");
				}
			}	
			else {
				System.out.println("Es ist ein Schaltjahr.");
			}
		}
		else {
			System.out.println("Es ist kein Schaltjahr.");
		}
	}

}
