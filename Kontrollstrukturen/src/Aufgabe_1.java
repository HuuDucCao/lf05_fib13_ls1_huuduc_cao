
public class Aufgabe_1 {

	public static void main(String[] args) {
		int x = 4;
		int y = 6;
		int z = 8;
		
		if (x == y) {
			System.out.println("Beide Zahlen sind gleich!");
		}
		else {
			System.out.println("Beide Zahlen sind unterschiedlich!");
		}
		
		if (x < y) {
			System.out.println("Die 2. Zahl ist gr��er!");
		}
		
		if (x >= y) {
			System.out.println("Die 1. Zahl ist gr��er oder gleich die 2. Zahl!");
		}
		else {
			System.out.println("Die 2. Zahl ist gr��er als die erste Zahl!");
		}
		
		System.out.println();
		
		if (x > y && x > z) {
			System.out.println("Die 1. Zahl ist gr��er als die 2. und die 3. Zahl!");
		}
		
		if (z > y || z > x) {
			System.out.println("Die 3. Zahl ist entweder gr��er als die 2. Zahl oder die 1. Zahl!");
		}
		
		if (x > y && x > z) { 
			System.out.println(x + " ist die gr��te Zahl!");
		}
		else if (y > x && y > z) {
			System.out.println(y + " ist die gr��te Zahl!");
		}
		else {
			System.out.println(z + " ist die gr��te Zahl!");
		}
	}

}
