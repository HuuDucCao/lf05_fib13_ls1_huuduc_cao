
public class Schleifen_2 {

	public static void main(String[] args) {
		int n = 1;
		while (n <= 10) {
			System.out.print(n + " ");
			n++;
		}
		
		System.out.println();
		
		int m = 10; 
		while(m > 0) {
			System.out.print(m + " ");
			m--;
		}
	}

}
