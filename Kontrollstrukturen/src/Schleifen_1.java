import java.util.Scanner;

public class Schleifen_1 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		for (int n = 1; n <= 10; n++) {
			System.out.print(n + " ");
		}
		
		System.out.println();
		
		for (int n = 10; n > 0; n--) {
			System.out.print(n + " ");
		}
		
		/*System.out.println();
		
		System.out.print("Geben Sie bitte einen begrenzenden Wert ein: ");
		int eingabe = myScanner.nextInt();
		
		for (int n = 0; n <= eingabe; n++) {
			int m1 =  n;
			System.out.print(m1 + " ");
		}
		
		System.out.println();
		
		int n = 0;
		while(n <= eingabe) {
			System.out.print(n + " ");
			n++;
		}
		
		for (int n = 0; n <= 10; n++) {
			int m = n + n;
			System.out.print(m + " ");
		}
		System.out.println("Die Summe f�r A betr�gt: "); */
		
		System.out.println();
		
		for (int n = 0; n <= 200; n++) {
			if (n % 7 == 0) {
				System.out.print(n + " ");
			}
			else if (n % 5 != 0 && n % 4 == 0) {
				System.out.print(n + " ");
			}
		}
	}

}
