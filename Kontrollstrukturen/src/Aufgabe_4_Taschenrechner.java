import java.util.Scanner;

public class Aufgabe_4_Taschenrechner {

	public static void main(String[] args) {
		double zahl1;
		double zahl2;
		String eingabe;
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Dies ist ein Taschenrechner. Bitte geben Sie die erste zahl ein: ");
		zahl1 = myScanner.nextDouble();
		System.out.println("Bitte geben Sie die zweite Zahl ein: ");
		zahl2 = myScanner.nextDouble();
		System.out.println("Bitte geben Sie die Rechenoperation ein. Zur Verf�gung sind +, -, *, /");
		eingabe = myScanner.next();
		
		switch (eingabe) {
		case "+":
			System.out.println(zahl1 + zahl2);
			break;
		case "-":
			System.out.println(zahl1 - zahl2);
			break;
		case "*":
			System.out.println(zahl1 * zahl2);
			break;
		case "/":
			System.out.println(zahl1 / zahl2);
			break;
		default:
			System.out.println("Die Eingabe war ung�ltig.");
		}
	}

}
