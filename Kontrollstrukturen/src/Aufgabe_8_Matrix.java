import java.util.Scanner;

public class Aufgabe_8_Matrix {

	public static void main(String[] args) {
		int eingabe;
		int i = 1;
		int j = 1;
		double k = 0;
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Dies ist ein Programm f�r die Anzeige einer Matrix. Bitte geben Sie eine Zahl zwischen 2 und 9 ein: ");
		eingabe = myScanner.nextInt();
		System.out.print("0 ");
		if (eingabe >= 2 && eingabe <= 9) {
			while(i < 100 && j < 100) {
				if(j % 10 == 0) {
					System.out.println(" ");
				}
				if(i % eingabe == 0) {
					System.out.print("* ");
				}
				else if (i == 1) {
					System.out.print("* ");
				}
				else {
					System.out.print(i + " ");
				}
				i = i + 1;
				j = j + 1;
				k = k + 1;
			}
		}
	}

}
