import java.util.Scanner;

public class Aufgabe_5_Ohmsches_Gesetz {

	public static void main(String[] args) {
		String eingabe;
		double r;
		double i;
		double u;
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie den Buchstaben der gesuchten Gr��e ein. Zur Auswahl stehen R, U und I: ");
		eingabe = myScanner.next();
		
		switch (eingabe) {
		case "R":
			System.out.println("R wurde eingegeben. Bitte geben Sie den Wert f�r I ein: ");
			i = myScanner.nextDouble();
			System.out.println("Bitte geben Sie den Wert f�r U ein: ");
			u = myScanner.nextDouble();
			System.out.println("R ist " + u / i + " gro�.");
			break;
		case "I":
			System.out.println("I wurde eingegeben. Bitte geben Sie den Wert f�r R ein: ");
			r = myScanner.nextDouble();
			System.out.println("Bitte geben Sie den Wert f�r U ein: ");
			u = myScanner.nextDouble();
			System.out.println("I ist " + u / r + " gro�.");
			break;	
		case "U":
			System.out.println("U wurde eingegeben. Bitte geben Sie den Wert f�r R ein: ");
			r = myScanner.nextDouble();
			System.out.println("Bitte geben Sie den Wert f�r I ein: ");
			i = myScanner.nextDouble();
			System.out.println("U ist " + r * i + " gro�.");
			break;
		default: 
			System.out.println("Die Eingabe war ung�ltig.");
		}
	}

}
