
public class Volumenberechnung {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double a = 3.0;
		double b = 2.0;
		double c = 4.0;
		double h = 5.0;
		double r = 7.0;
		
		System.out.print("Würfelberechnung: ");
		System.out.println(wuerfel(a));
		System.out.print("Quaderberechnung: ");
		System.out.println(quader(a, b, c));
		System.out.print("Pyramidenberechnung: ");
		System.out.println(pyramide(a, h));
		System.out.print("Kugelberechnung: ");
		System.out.println(kugel(r));

	}
	static double wuerfel(double w) {
		double wb = w * w * w;
		return wb;
	}
	
	static double quader(double q1, double q2, double q3) {
		double qb = q1 * q2 * q3;
		return qb;
	}
	
	static double pyramide(double p1, double p2) {
		double pb = p1 * p1 * p2 / 3;
		return pb;
	}
	
	static double kugel(double k) {
		double kb = 4/3 * (k * k * k) * 3.1416;
		return kb;
	}
}
