import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		
		// Benutzereingaben lesen
		String artikel = liesString("Was m�chten Sie bestellen?");
		System.out.println("Sie haben " + artikel + " zu Ihrem Warenkorb hinzugef�gt!");
	
		int anzahl = liesInt("Geben Sie die Anzahl ein: ");
		System.out.println("Die Anzahl betr�gt " + anzahl + "!");

		double nettopreis = liesDouble("Geben Sie den Nettopreis ein:" );
		System.out.println("Der Nettopreis betr�gt " + nettopreis + " Euro!");

		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:" );
		System.out.println("Diw Mwst betr�gt " + mwst + "%!");
		
		double gesamtnettopreis = berechneGesamtnettopreis(anzahl, nettopreis);
		System.out.println("Der Gesamtnettopreis betr�gt " + berechneGesamtnettopreis(anzahl, nettopreis) + " Euro!");
		double gesamtbruttopreis = berechneGesamtbruttopreis(gesamtnettopreis, mwst);
		System.out.println("Der Gesamtbruttopreis betr�gt " + berechneGesamtbruttopreis(gesamtnettopreis, mwst) + " Euro!");
		
		//Ausgeben

		rechungsausgeben(artikel, anzahl, gesamtnettopreis, gesamtbruttopreis, mwst);

	//Verarbeitung
	}
	public static String liesString(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		String ls = myScanner.next();
		return ls;
	}
	
	public static int liesInt(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		int li = myScanner.nextInt();
		return li;
	}
	
	public static double liesDouble(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		double ld = myScanner.nextDouble();
		return ld;
	}
	//Verarbeitung 
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		double bnp = anzahl * nettopreis;
		return bnp;
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		double bgp = nettogesamtpreis * (1 + mwst / 100);
		return bgp;
	}
	//Ausgabe 
	public static void rechungsausgeben(String artikel, int anzahl, double gesamtnettopreis, double bruttogesamtpreis, double mwst) {
		System.out.print("Ihre Rechnung:");
		System.out.printf("\t Netto:  %-10s %6d St�ck %10.2f Euro %n", artikel, anzahl, gesamtnettopreis);
		System.out.printf("\t\t Brutto: %-10s %6d St�ck %10.2f Euro (%.1f%s) Mwst%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
}
