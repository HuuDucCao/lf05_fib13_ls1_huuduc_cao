import java.util.Scanner;

class Test1
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneM�nze;
       double rueckgabebetrag;
       double summe;
       byte anzahlTicket;
       
       zuZahlenderBetrag = fahrkartenbestellungErfassen();
       rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       fahrkartenAusgeben();
       rueckgeldAusgeben(rueckgabebetrag);
       
       
       System.out.printf("Sie m�ssen %.2f Euro zahlen!");
       System.out.println("");

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < summe)
       {
    	   System.out.printf("Noch zu zahlen: " + "%.2f Euro", (summe - eingezahlterGesamtbetrag));
    	   System.out.println("");
    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
    	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rueckgabebetrag = eingezahlterGesamtbetrag - summe;
       if(rueckgabebetrag > 0.0)
       {
    	   System.out.printf("Der R�ckgabebetrag in H�he von "  + " %.2f EURO", rueckgabebetrag);
    	   System.out.println("");
    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

           while(rueckgabebetrag >= 2.0) // 2 EURO-M�nzen
           {
        	  System.out.println("2 EURO");
	          rueckgabebetrag -= 2.0;
           }
           while(rueckgabebetrag >= 1.0) // 1 EURO-M�nzen
           {
        	  System.out.println("1 EURO");
	          rueckgabebetrag -= 1.0;
           }
           while(rueckgabebetrag >= 0.5) // 50 CENT-M�nzen
           {
        	  System.out.println("50 CENT");
	          rueckgabebetrag -= 0.5;
           }
           while(rueckgabebetrag >= 0.2) // 20 CENT-M�nzen
           {
        	  System.out.println("20 CENT");
 	          rueckgabebetrag -= 0.2;
           }
           while(rueckgabebetrag >= 0.1) // 10 CENT-M�nzen
           {
        	  System.out.println("10 CENT");
	          rueckgabebetrag -= 0.1;
           }
           while(rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
           {
        	  System.out.println("5 CENT");
 	          rueckgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
    }
    public static double fahrkartenbestellungErfassen() {
    	Scanner myScanner = new Scanner(System.in);
    	System.out.println("Willkommen! Dies ist ein Fahrkartenautomat. Wie viele Fahrkarten m�chte Sie bestellen?");
    	double karten = myScanner.nextDouble();
        System.out.println("Sie haben " + karten + " Fahrkarten bestellt!");
        
        System.out.println("Wie viel kostet ein Ticket?");
        double kosten = myScanner.nextDouble();
        System.out.println("Ein Ticket kostet nun " + kosten + " Euro!");
    	double fbe = karten * kosten;
        return fbe;
    }
    
    public static double fahrkartenBezahlen(double zuZahlen) {

        anzahlTicket = tastatur.nextByte(); 
    	return fkb;
    }
    
    public static void fahrkartenAusgeben() {
    	
    }
    
    public static void rueckgeldAusgeben(double a) {
    	
    }
}
//Aufgabe 5
//F�r alle Summen und Eingaben habe ich double verwendet, da es Nachkommastellen beinhaltet und man den Europreis
//in Komma schreiben kann. F�r die Anzahl der Fahrkarten habe ich mich f�r byte entschieden, da diese bis 127 geht und
//ich nicht davon ausgehe, dass jemand in einer einzigen Transaktion mehr als 127 Fahrkarten kauft.

//Aufgabe 6
//Die beiden Variablen multiplizieren sich miteinander und das Ergebnis wird in der neuen Variablen, was in diesem
//Falle "summe", gespeichert. Die neue Variable beinhaltet nun den neuen Wert und man kann damit weiterarbeiten.
//Besagte Variable wird in den folgenden Funktionen bzw. Befehlen genutzt, um die Summe des Preises auszugeben und
//als Teil einer Rechnung. 