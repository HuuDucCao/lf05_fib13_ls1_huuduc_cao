import java.util.Scanner;

public class Aufgabe3_3
{	
	public static void main(String[] args) {
	double zuZahlenderBetrag;
	double rueckgabebetrag;
	int anzahl;
	
	anzahl = ticketAnzahl();
	zuZahlenderBetrag = fahrkartenbestellungErfassen(anzahl);
	rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	fahrkartenAusgeben(anzahl);
	rueckgeldAusgeben(rueckgabebetrag, anzahl);
	}

//-----------------------------------------------------------------------------------------
// Code der die Anzahl der Tickets z�hlt und diese dann ausgibt. Wird ben�tigt, da ich in verschiedenen Methoden mit der Ticket Anzahl arbeite.
		
	public static int ticketAnzahl() {
		Scanner tastatur = new Scanner(System.in);
		int anzahl;
	
		System.out.println("Willkommen! Dies ist ein Fahrkartenautomat. Wie viele Fahrkarten m�chten Sie bestellen?");
		anzahl = tastatur.nextInt();
		System.out.println("Sie haben " + anzahl + " Fahrkarten bestellt!");
	
		return anzahl;
	}
	
//-----------------------------------------------------------------------------------------
// Code der die Anzahl und den Preis von Tickets multipliziert und als Wert ausgibt.
	
	public static double fahrkartenbestellungErfassen(double x) {
		Scanner myScanner = new Scanner(System.in);
        
        System.out.println("Wie viel kostet ein Ticket?");
        double preis = myScanner.nextDouble();
    	return(x * preis);
	}
	
//-----------------------------------------------------------------------------------------
// Code der sich um das Bezahlen des Tickets k�mmert und berechnet ob und wieviel R�ckgeld dem Kunden zusteht.
	
	public static double fahrkartenBezahlen(double x) {
	Scanner tastatur = new Scanner(System.in);
	double geld;
	
	System.out.println("\nDer Preis f�r die Tickets liegt bei: " + x + " EURO!"); 
	System.out.println("Bitte bezahlen Sie den Betrag in 5 Cent bis 2 EURO-Münzen");
    while(x > 0.0)
    {
    	geld = tastatur.nextDouble();
    	x = x - geld;
    	if(x > 0.0)
    	{
    		System.out.println("\nSie haben " + geld + " EURO eingeworfen!\n");
    		System.out.println("Es fehlen noch " + x + " EURO!");
    	}
    }
    if(x < 0.0)
    {
    	x = (x * -1);
    	System.out.println("Ihnen werden " + x + " EURO ausgegeben");
    }
    
    return x;
	}
	
//-----------------------------------------------------------------------------------------
// Code der den Kunden informiert, dass das Ticket, bzw. die Tickets, gedruckt werden.
	
	public static void fahrkartenAusgeben(double x) {
	
	if (x <= 1)
	{
		System.out.println("\nFahrschein wird gedruckt");
	}
	else 
	{
		System.out.printf("\n"+ "%.0f" + " Fahrscheine werden gedruckt\n\n" , x);   
	}
	for (int i = 0; i < 8; i++)
	{
		System.out.print("=");
	    try {
	    	Thread.sleep(250);
			} 
	    catch (InterruptedException e) {
			e.printStackTrace();
			}
	}
	}
	
//-----------------------------------------------------------------------------------------
// Code der das R�ckkeld ausgibt und den Kauf abschlie�t
	
	public static void rueckgeldAusgeben(double x, double y) {
	
    if(x > 0.0)
    {
 	   System.out.printf("\n\nDer R�ckgabebetrag in H�he von" + " %.2f EURO ", (x));
 	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

        while(x >= 2.0) // 2 EURO-M�nzen
        {
     	  System.out.println("2 EURO");
	          x -= 2.0;
        }
        while(x >= 1.0) // 1 EURO-M�nzen
        {
     	  System.out.println("1 EURO");
	          x -= 1.0;
        }
        while(x >= 0.5) // 50 CENT-M�nzen
        {
     	  System.out.println("50 CENT");
	          x -= 0.5;
        }
        while(x >= 0.2) // 20 CENT-M�nzen
        {
     	  System.out.println("20 CENT");
	          x -= 0.2;
        }
        while(x >= 0.1) // 10 CENT-M�nzen
        {
     	  System.out.println("10 CENT");
	          x -= 0.1;
        }
        while(x >= 0.05)// 5 CENT-M�nzen
        {
     	  System.out.println("5 CENT");
	          x -= 0.05;
        }
    }
    if (y <= 1)
    {
 	   System.out.println("\nVergessen Sie nicht, den Fahrschein");
    }
    else 
    {
 	   System.out.println("\nVergessen Sie nicht, die Fahrscheine");
    }
    System.out.println("vor Fahrtantritt entwerten zu lassen!\n"+
                       "Wir w�nschen Ihnen eine gute Fahrt.");
 }
}