import java.util.Scanner;													// Scanner utility wird importiert

class Fahrkartenautomat_Aufgabe_2_5_EingabePreis							// Name der Klasse
{
    public static void main(String[] args)									// Standardger�st
    {
       Scanner tastatur = new Scanner(System.in);							// Erstellung Variable Tastatur mit Eingabe des Users als Inhalt
      
       double zuZahlenderBetrag; 											// Deklarierung der Variable zuZahlenderBetrag als Double (Kommazahl)
       double eingezahlterGesamtbetrag;										// Deklarierung der Variable eingezahlterGesamtbetrag als Double (Kommazahl)
       double eingeworfeneM�nze;											// Deklarierung der Variable eingeworfeneM�nze als Double (Kommazahl)
       double r�ckgabebetrag;												// Deklarierung der Variable r�ckgabebetrag als Double (Kommazahl)
       double preisFahrkarte;												// Deklarierung der Variable preisFahrkarte mit festgelegtem Wert von 3.50
       byte anzahlFahrkarten;												// Deklarierung der Variable anzahlFahrkarten als Integer (Ganzzahl)
       
       System.out.println("Guten Tag, willkommen bei der BVG! ");			// gib Begr��ung von BVG aus und springe in n�chste Zeile
       
       System.out.print("Bitte geben Sie einen Preis f�r das Ticket ein, welches Sie kaufen m�chten (Bitte nur in 5 CENT [0,05] Schritten): "); 		// Nachfrage nach Preis des Tickets und springe in n�chste Zeile
       preisFahrkarte = tastatur.nextDouble();
       
       System.out.println("");												// gebe leere Zeile aus
       
       System.out.print("Wie viele Fahrkarten m�chten Sie kaufen? (maximal 127 Tickets): ");		// gib Nachfrage zwecks Anzahl von Tickets aus
       anzahlFahrkarten= tastatur.nextByte();														// einlesen von eingegebener Fahrkartenanzahl
       
       System.out.println(" ");												// gib eine leere Zeile aus
       
       zuZahlenderBetrag = preisFahrkarte * anzahlFahrkarten;				// Initialisierung der Variable zuZahlenderBetrag welche dem Ergebnis preisFahrkarte * anzahlFahrkarten entspricht 

       System.out.printf("Die von Ihnen eingegebene Anzahl an Fahrkarten kostet insgesamt: " + "%.2f EURO \n", zuZahlenderBetrag);		// Ausgabe von "Die von Ihnen eingegebene Anzahl an Fahrkarten kostet insgesamt:" in EURO und mit 2 Kommastellen 
       
       System.out.println(" ");												// gib eine leere Zeile aus
       
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;										// Initialisierung der Variable eingezahlterGesamtbetrag mit Startwert 0.0
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)					// SOLANGE Variable eingezahlterGesamtbetrag < zuZahlenderBetrag
       {
    	   System.out.printf("Es sind noch " + "%.2f EURO zu zahlen! \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));		// gib aus "Es sind noch ... EURO zu zahlen: " mit verrechnetem Restwert
    	   System.out.println(" ");
    	   System.out.print("Eingabe (mind. 5Ct [Eingabe = 0,05], h�chstens 10 Euro [Eingabe = 10]): ");						// gib aus "Eingabe (mind. 5Ct [Eingabe = 0,05], h�chstens 10 Euro [Eingabe = 10]): "
    	   eingeworfeneM�nze = tastatur.nextDouble();																			// Initialisierung der Variable eingeworfene M�nze als Eingabe des Users als Kommazahl
           eingezahlterGesamtbetrag += eingeworfeneM�nze;																		// addiere zu Variable eingezahlterGesamtbetrag den Wert von der Variable eingeworfene M�nze dazu
       }

       // Fahrscheinausgabe																					// NEUE FUNKTION
       // -----------------
       System.out.println("\nFahrschein wird gedruckt:");													// gib aus Fahrschein wird ausgegeben
       for (int i = 0; i < 8; i++)																			// Variable i wird als integer deklariert mit dem Wert 0 (int i = 0), solange i < 8 ist, Wert 1 hinzuz�hlen (i++)
       {
          System.out.print("=");																			// gib "=" aus
          try {
			Thread.sleep(250);																				// Verz�gerung beim ausgeben damit ein Ladeeffekt erzeugt wird
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");																			// 2 leere Zeilen werden untereinander ausgegeben

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;										// Variable r�ckgabebetrag wird initialisiert und soll mit der Verrechnung von eingezahlterGesamtbetrag - zuZahlenderBetrag gef�llt werden
       if(r�ckgabebetrag > 0.0)																				// falls r�ckgabebetrag > 0.0 dann... 
       {
    	   System.out.printf("Der R�ckgabebetrag in H�he von: \n" + "%.2f EURO", r�ckgabebetrag);			// gib aus "Der R�ckgabebetrag in H�he von: " (mit einem horizentalen tab) + r�ckgabebetrag mit 2 Kommastellen + " EURO"
    	   System.out.println("\n");																		// gib aus Leerzeile
    	   System.out.println("Dieser wird in folgenden M�nzen ausgezahlt: \n");									// gib aus "Wird in folgenden M�nzen ausgezahlt: " (mit einem horizentalen tab))
    	       	   
           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen													// wenn r�ckgabebetrag >= 2.0 dann
           {
        	  System.out.println("2,00 EURO");																// gib aus "2 EURO"
	          r�ckgabebetrag -= 2.0;																		// ziehe den Wert 2.0 von der Variable r�ckgabebetrag ab
           }
           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen													// wenn r�ckgabebetrag >= 1.0 dann
           {
        	  System.out.println("1,00 EURO");																// gib aus "1 EURO"
	          r�ckgabebetrag -= 1.0;																		// ziehe den Wert 1.0 von der Variable r�ckgabebetrag ab
           }
           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen													// wenn r�ckgabebetrag >= 0.5 dann
           {
        	  System.out.println("0,50 CENT");																// gib aus "50 CENT"
	          r�ckgabebetrag -= 0.5;																		// ziehe den Wert 0.5 von der Variable r�ckgabebetrag ab
           }
           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen													// wenn r�ckgabebetrag >= 0.2 dann
           {
        	  System.out.println("0,20 CENT");																// gib aus "20 CENT"
 	          r�ckgabebetrag -= 0.2;																		// ziehe den Wert 0.2 von der Variable r�ckgabebetrag ab
           }
           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen													// wenn r�ckgabebetrag >= 0.1 dann
           {
        	  System.out.println("0,10 CENT");																// gib aus "10 CENT"
	          r�ckgabebetrag -= 0.1;																		// ziehe den Wert 0.1 von der Variable r�ckgabebetrag ab
           }
           while(r�ckgabebetrag >= 0.05) // 5 CENT-M�nzen													// wenn r�ckgabebetrag >= 0.05 dann
           {
        	  System.out.println("0,05 CENT");																// gib aus "5 CENT"
 	          r�ckgabebetrag -= 0.05;																		// ziehe den Wert 0.05 von der Variable r�ckgabebetrag ab
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+										// gib aus...
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
       
       
       // Aufgabe 2.5 Erster Teil Variablen und dessen Datentypen
       
       // Variablen					|	Datentypen
       // zuZahlenderBetrag			|	double
       // eingezahlterGesamtbetrag	|	double
       // eingeworfeneM�nze			|	double
       // r�ckgabebetrag			|	double
       
       // preisFahrkarte			|	double
       // anzahlFahrkarten			|	byte
       
       
       // Aufgabe 2.5 Zweiter Teil Welche Operationen werden mit den Variablen durchgef�hrt

       // 1. (Variable eingezahlterGesamtbetrag wird mit dem Wert 0.0 initialisiert)

       // 2. boolische Abfrage:		ob es wahr ist, dass eingezahlterGesamtbetrag < zuZahlenderBetrag

       // 3. Subtraktion:			zuZahlenderBetrag - eingezahlterGesamtbetrag

       // 4. Addition:
       // zur Bestimmung (Initialisierung) der Variable eingezahlterGesamtbetrag
       // wird der zuvor eingelesene Wert aus der Variable eingeworfeneM�nze zum Wert innerhalb der Variable eingezahlterGesamtbetrag addiert

       // 5. Subtraktion:
       // zur Bestimmung (Initialisierung) der Variable r�ckgabebetrag wird eingezahlterGesamtbetrag - zuZahlenderBetrag berechnet

       // 6. diverse boolesche Abfragen mit der Variable r�ckgabebetrag:
       // Pr�fung ob der Wert innerhalb von der Variable r�ckgabebetrag: > 0.0 (ist gr��er 0) und >= (ist gr��er gleich) die Werte 2.0, 1.0, 0.5, 0.2, 0.1, 0.05 
       
       // Aufgabe 2.5 F�nfter Teil Begr�ndung des gew�hlten Datentyps bei der Variable anzahlFahrkarten#
       
       // Begr�ndung Datentyp

       // Ich habe mich bei der Variable anzahlFahrkarten f�r den Datentyp Byte entschieden, da dieser die Werte von -128 bis 127 enth�lt.
       // Dieser Wertebereich ist aus meiner Sicht mehr als ausreichen, da es nicht realistisch ist, dass jemand an einem Fahrkartenautomaten mehr als 127 Tickets kauft
       // (auch allein 127 Tickets ist unrealistisch an einem Fahrkartenautomaten)

       // Aufgabe 2.5 Sechster Teil Erl�uterung Berechnung des Ausdrucks anzahl * einzelpreis (bei mir preisFahrkarte * anzahlFahrkarten)
       
       // Vorab ist kurz zu erw�hnen, dass in die Variablen preisFahrkarte und anzahlFahrkarten nacheinander Werte vom Nutzer eingelesen und entsprechend gespeichert wurden (Initialisierung der Variablen).
       // Bei der eigentlichen Berechnung werden die zuvor initialisierten Werte innerhalb der Variablen miteinander multipliziert.
       // Der nach dieser Berechnung erhaltene Wert wird wiederum in der Variable zuZahlenderBetrag gespeichert bzw. die Variable zuZahlenderBetrag wird mit diesem errechnetem Wert initialisiert.
       // Allgemein ist festzuhalten, dass das Programm in diesem Fall nicht wie sonst �blich den code von links nach rechts lie�t und bearbeitet
       // SONDERN wie bereits erw�hnt von rechts nach links vorgeht.
       // (erst Verrechnung der Werte innerhalb der Variablen preisFahrkarte und anzahlFahrkarten und dann die Variable zuZahlenderBetrag initialisieren)


    }
}