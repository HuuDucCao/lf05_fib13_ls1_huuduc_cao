import java.util.Scanner;

class Aufgabe2_5
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneM�nze;
       double r�ckgabebetrag;
       double summe;
       byte anzahlTicket;

       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();
       
       System.out.print("Anzahl der Tickets: ");
       anzahlTicket = tastatur.nextByte(); 
       
       summe = zuZahlenderBetrag * anzahlTicket;
       
       System.out.printf("Sie m�ssen %.2f Euro zahlen!", summe);
       System.out.println("");

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < summe)
       {
    	   System.out.printf("Noch zu zahlen: " + "%.2f Euro", (summe - eingezahlterGesamtbetrag));
    	   System.out.println("");
    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
    	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       r�ckgabebetrag = eingezahlterGesamtbetrag - summe;
       if(r�ckgabebetrag > 0.0)
       {
    	   System.out.printf("Der R�ckgabebetrag in H�he von "  + " %.2f EURO", r�ckgabebetrag);
    	   System.out.println("");
    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
           {
        	  System.out.println("2 EURO");
	          r�ckgabebetrag -= 2.0;
           }
           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
           {
        	  System.out.println("1 EURO");
	          r�ckgabebetrag -= 1.0;
           }
           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
           {
        	  System.out.println("50 CENT");
	          r�ckgabebetrag -= 0.5;
           }
           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
           {
        	  System.out.println("20 CENT");
 	          r�ckgabebetrag -= 0.2;
           }
           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
           {
        	  System.out.println("10 CENT");
	          r�ckgabebetrag -= 0.1;
           }
           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
           {
        	  System.out.println("5 CENT");
 	          r�ckgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
    }
}
//Aufgabe 5
//F�r alle Summen und Eingaben habe ich double verwendet, da es Nachkommastellen beinhaltet und man den Europreis
//in Komma schreiben kann. F�r die Anzahl der Fahrkarten habe ich mich f�r byte entschieden, da diese bis 127 geht und
//ich nicht davon ausgehe, dass jemand in einer einzigen Transaktion mehr als 127 Fahrkarten kauft.

//Aufgabe 6
//Die beiden Variablen multiplizieren sich miteinander und das Ergebnis wird in der neuen Variablen, was in diesem
//Falle "summe", gespeichert. Die neue Variable beinhaltet nun den neuen Wert und man kann damit weiterarbeiten.
//Besagte Variable wird in den folgenden Funktionen bzw. Befehlen genutzt, um die Summe des Preises auszugeben und
//als Teil einer Rechnung. 