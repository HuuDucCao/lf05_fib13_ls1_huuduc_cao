import java.util.Scanner;

public class Aufgabe_4_5
{	
	public static void main(String[] args) {
	double zuZahlenderBetrag;
	double rueckgabebetrag;
	int anzahl;
	String eingabe;
	Scanner tastatur = new Scanner(System.in);
	
	System.out.println("Willkommen! Dies ist ein Fahrkartenautomat. \nWenn Sie den Fahrkartenautomat benutzen wollen, tippen Sie Ja ein. \nWenn Sie es beenden wollen, dann tippen Sie Nein ein.");
	eingabe = tastatur.next();
	if(eingabe.equals("Nein")) {
		System.out.println("Wir w�nschen Ihnen einen angenehmen Tag!");
	}
	else if(eingabe.equals("Ja")) {
		
	}
	else {
		System.out.println("Die Eingabe war ung�ltig.");
	}
	
	while(eingabe.equals("Ja")) {
		anzahl = ticketAnzahl();
		zuZahlenderBetrag = fahrkartenbestellungErfassen(anzahl);
		rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben(anzahl);
		rueckgeldAusgeben(rueckgabebetrag, anzahl);
		System.out.println("M�chten Sie weitere Fahrkarten kaufen?");
		eingabe = tastatur.next();
		if(eingabe.equals("Ja")) {
			
		}
		else if(eingabe.equals("Nein")) {
			System.out.println("Wir w�nschen Ihnen einen angenehmen Tag!");
			break;
		}
		else {
			System.out.println("Die Eingabe war ung�ltig.");
			break;
		}
		
	}
}

//-----------------------------------------------------------------------------------------
// Code der die Anzahl der Tickets z�hlt und diese dann ausgibt. Wird ben�tigt, da ich in verschiedenen Methoden mit der Ticket Anzahl arbeite.
		
	public static int ticketAnzahl() {
		Scanner tastatur = new Scanner(System.in);
		int anzahl;
	
		System.out.println("Wie viele Fahrkarten m�chten Sie bestellen?");
		anzahl = tastatur.nextInt();
		System.out.println("Sie haben " + anzahl + " Fahrkarten bestellt!");
	
		return anzahl;
	}
	
//-----------------------------------------------------------------------------------------
// Code der die Anzahl und den Preis von Tickets multipliziert und als Wert ausgibt.
	
	public static double fahrkartenbestellungErfassen(double x) {
		Scanner myScanner = new Scanner(System.in);
        
        int eingabe = 0; 
        double preis = 0;
    	System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:");
    	System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
    	System.out.println("Tageskarte Regeltarif AB [8,60 EUR] (2)");
    	System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
    	eingabe = myScanner.nextInt();
    	switch (eingabe) {
    	case 1:
    		preis = 2.90 * x; 
    		break;
    	case 2:
    		preis = 8.60 * x;
    		break;
    	case 3:
    		preis = 23.50 * x;
    		break;
    	default:
    		System.out.println("Ihre Eingabe war ung�ltig.");
    		break;
    	} 
    	return preis;
	}
	
//-----------------------------------------------------------------------------------------
// Code der sich um das Bezahlen des Tickets k�mmert und berechnet ob und wieviel R�ckgeld dem Kunden zusteht.
	
	public static double fahrkartenBezahlen(double x) {
	Scanner tastatur = new Scanner(System.in);
	double geld;
	
	System.out.println("\nDer Preis f�r die Tickets liegt bei: " + x + " EURO!"); 
	System.out.println("Bitte bezahlen Sie den Betrag in 5 Cent bis 2 EURO-Münzen");
    while(x > 0.0)
    {
    	geld = tastatur.nextDouble();
    	x = x - geld;
    	if(x > 0.0)
    	{
    		System.out.println("\nSie haben " + geld + " EURO eingeworfen!\n");
    		System.out.println("Es fehlen noch " + x + " EURO!");
    	}
    }
    if(x < 0.0)
    {
    	x = (x * -1);
    	System.out.println("Ihnen werden " + x + " EURO ausgegeben");
    }
    
    return x;
	}
	
//-----------------------------------------------------------------------------------------
// Code der den Kunden informiert, dass das Ticket, bzw. die Tickets, gedruckt werden.
	
	public static void fahrkartenAusgeben(double x) {
	
	if (x <= 1)
	{
		System.out.println("\nFahrschein wird gedruckt");
	}
	else 
	{
		System.out.printf("\n"+ "%.0f" + " Fahrscheine werden gedruckt\n\n" , x);   
	}
	for (int i = 0; i < 8; i++)
	{
		System.out.print("=");
	    try {
	    	Thread.sleep(250);
			} 
	    catch (InterruptedException e) {
			e.printStackTrace();
			}
	}
	}
	
//-----------------------------------------------------------------------------------------
// Code der das R�ckkeld ausgibt und den Kauf abschlie�t
	
	public static void rueckgeldAusgeben(double x, double y) {
	
    if(x > 0.0)
    {
 	   System.out.printf("\n\nDer R�ckgabebetrag in H�he von" + " %.2f EURO ", (x));
 	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

        while(x >= 2.0) // 2 EURO-M�nzen
        {
     	  System.out.println("2 EURO");
	          x -= 2.0;
        }
        while(x >= 1.0) // 1 EURO-M�nzen
        {
     	  System.out.println("1 EURO");
	          x -= 1.0;
        }
        while(x >= 0.5) // 50 CENT-M�nzen
        {
     	  System.out.println("50 CENT");
	          x -= 0.5;
        }
        while(x >= 0.2) // 20 CENT-M�nzen
        {
     	  System.out.println("20 CENT");
	          x -= 0.2;
        }
        while(x >= 0.1) // 10 CENT-M�nzen
        {
     	  System.out.println("10 CENT");
	          x -= 0.1;
        }
        while(x >= 0.05)// 5 CENT-M�nzen
        {
     	  System.out.println("5 CENT");
	          x -= 0.05;
        }
    }
    if (y <= 1)
    {
 	   System.out.println("\nVergessen Sie nicht, den Fahrschein");
    }
    else 
    {
 	   System.out.println("\nVergessen Sie nicht, die Fahrscheine");
    }
    System.out.println("vor Fahrtantritt entwerten zu lassen!\n"+
                       "Wir w�nschen Ihnen eine gute Fahrt.");
 }
}